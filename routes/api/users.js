const express=require('express');
const {check,validationResult}=require('express-validator')
const gravatar=require('gravatar')
const User= require('../../models/User')
const bcrypt = require('bcryptjs')
const jwt=require('jsonwebtoken')
const router = express.Router();
const config=require('config');

router.post('/',[
    check('name','name is required').not().isEmpty(),
    check('email','Please enter valid email').isEmail(),
    check('password','please enter a password with 6 or more').isLength({min:6}),
    check('status','status is required').not().isEmpty()
],async(req,res)=>{
    const errors=validationResult(req);
    if(!errors.isEmpty()){
        return res.status(400).json({errors:errors.array()})
    }
    const {name,email,password,status}=req.body;
    try{
        //user already exists
        let user=await User.findOne({email});
        if(user){
            return res.status(400).json({errors:[{msg:'user already exists'}]})
        }
        // get user gravator
        const avatar=gravatar.url(email,{
            s:'200',
            r:'pg',
            d:'mm'
        })
        user =await new User({
            name,
            email,
            password,
            status,
            avatar
        });
        // Encrypt password
        const salt=await bcrypt.genSalt(10);
        user.password=await bcrypt.hash(password,salt)
        await user.save()
         // jwt
         const payload={
            user:{
                id:user.id
            }
        }
        jwt.sign(
            payload,
            config.get('jwtToken'),
            {expiresIn:360000},
            (err,token)=>{
                if(err)
                throw err;
                res.json({token})
            }
        )
        // res.status(200).send("succesful!");
    } catch(err){
        console.error(err.message);
        res.status(500).send('server error');
    }
    
    
});


module.exports=router;
