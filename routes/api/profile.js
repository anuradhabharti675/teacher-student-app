const express=require('express');
const router = express.Router();
const auth =require('../../middleware/auth')
const Profile =require('../../models/profile')
const User=require('../../models/User')
const {check, validationResult}=require('express-validator');
// @route GET/api/profile/me
router.get('/me',auth,async(req,res)=>{
  try{ 


  const profile = await Profile.findOne({ user: req.user.id })
  const profileDetail=await User.findById(req.user.id)
      if (!profile) {
        res.status(404).json({mgs:'There is no profile for this user'});
      }

      res.json({profile:profile,profile_detail:profileDetail});

  }catch(err){
      console.error(err.message);
      res.status(500).send('Server Error')
  }
});
// @route GET api/profile/students
// @desc Get students
router.get('/students',auth,async(req,res)=>{
    try{ 
    const profile = await Profile.findOne({ user: req.user.id })
    const studentDetail=await Student.findById(profile.students.id)
    
        if (!profile) {
          res.status(404).json({mgs:'There is no profile for this user'});
        }
  
        res.json({students:studentDetail});

    }catch(err){
        console.error(err.message);
        res.status(500).send('Server Error')
    }
});

//@route post api/profile
// @desc add instructor detail
router.post(
      '/',
      [
         auth,
         [
          
             check('skills','Skills is required')
              .not()
              .isEmpty(),
              check('institue','institute required')
        ]
      ],
      async(req,res)=>{
        const errors=validationResult(req);
        if(!errors.isEmpty()){
            return res.status(400).json({errors:errors.array()});
         }

         const{
             institute,
             website,
             location,
             bio,
             githubusername,
             skills,
             youtube,
             facebook,
             twitter,
             instagram,
             linkedin
         }=req.body;
    // Build profile object
    const profileFields={};
        profileFields.user=req.user.id;
        if(institute) profileFields.institute=institute;
        // if (website) profileFields.website = website;
        // if (location) profileFields.location = location;
        // if (bio) profileFields.bio = bio;
        // if (status) profileFields.status = status;
        // if (githubusername)
        //   profileFields.githubusername = githubusername;
        // Skills - Spilt into array
        if (typeof skills !== 'undefined') {
          profileFields.skills = skills.split(',');
        }
        console.log(profileFields.skills);
      try{
          let profile=await Profile.findOne({user:req.user.id})
          if(profile){
              profile=await Profile.findOneAndUpdate(
                  {user:req.user.id},
                  {$set:profileFields},
                  {new:true}
              )
              return res.json(profile)
          }
        //  create
        profile=new Profile(profileFields);
        await profile.save()
        res.json(profile)
      }catch(err){
          console.error(err.message);
          res.status(500).send('Server Error')
      }
 
    }

)

//@route post api/profile/student_id
// @desc add new student
router.post('/:student_id',auth,
  async(req,res)=>{
  try{
    const studentId=await User.findById(req.params.student_id)
    if(!studentId) 
      return res.send('User not valid') 
    let profile=await Profile.findOne({user:req.user.id})
    profile.students=studentId;
    await profile.save()
    res.json(profile)
  }catch(err){
      console.error(err.message);
      res.status(500).send('Server Error')
  }
}
)
//@route delete api/profile/student_id
// @desc delete instructor
router.delete('/:student_id',auth,
  async(req,res)=>{
  try{
    const studentId=await User.findById(req.params.student_id)
    if(!studentId) 
      return res.send('User not valid') 
    let profile=await Profile.findOne({user:req.user.id})
    delete profile.instructors.studentId;
    await profile.save()
    res.json(profile)
  }catch(err){
      console.error(err.message);
      res.status(500).send('Server Error')
  }

}

)
// @route GET api/profile/instructors
// @desc Get All instructors

router.get('/instructors',async(req,res)=>{
    try{
        const profiles= await Profile.find().populate('users',['name','avatar','email']);
        const profilesView=profiles.map(profile=>{
          return {
            id:profile.id,
            name:profile.name,
            email:profile.email,
            avatar:profile.avatar,
            skills:profile.skills,
            institute:profile.institute
        }})
         return res.json(profilesView);
    }catch(err){
        console.error(err.message);
        res.status(500).send('Server Error');
    }
})

module.exports=router;