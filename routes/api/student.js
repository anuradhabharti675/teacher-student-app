const express=require('express');
const {check,validationResult}=require('express-validator')
const gravatar=require('gravatar')
const Student= require('../../models/Student')
const User = require('../../models/User')
const bcrypt = require('bcryptjs')
const jwt=require('jsonwebtoken')
const router = express.Router();
const config=require('config');
const auth =require('../../middleware/auth')

// @route GET api/student
// @desc Get student
router.post('/',[
    check('name','name is required').not().isEmpty(),
    check('email','Please enter valid email').isEmail(),
    check('password','please enter a password with 6 or more').isLength({min:6})
],async(req,res)=>{
    const errors=validationResult(req);
    if(!errors.isEmpty()){
        return res.status(400).json({errors:errors.array()})
    }
    const {name,email,password}=req.body;
    try{
        //user already exists
        let student=await Student.findOne({email});
        if(student){
            return res.status(400).json({errors:[{msg:'user already exists'}]})
        }
        // get user gravator
        const avatar=gravatar.url(email,{
            s:'200',
            r:'pg',
            d:'mm'
        })
        student =await new Student({
            name,
            email,
            password,
            avatar
        });
        // Encrypt password
        const salt=await bcrypt.genSalt(10);
        student.password=await bcrypt.hash(password,salt)
        await student.save()
         // jwt
         const payload={
            student:{
                id:student.id
            }
        }
        jwt.sign(
            payload,
            config.get('jwtToken'),
            {expiresIn:360000},
            (err,token)=>{
                if(err)
                throw err;
                res.json({token})
            }
        )
        // res.status(200).send("succesful!");
    } catch(err){
        console.error(err.message);
        res.status(500).send('server error');
    }
    
    
});
// @route GET api/student/me
// @desc Get student

router.get('/me',auth,async(req,res)=>{
    try{ 
  
  
    const profile = await Student.findOne({ user: req.user.id })
    const profileDetail=await User.findById(req.user.id)
        if (!profile) {
          res.status(404).json({mgs:'There is no profile for this user'});
        }
  
        res.json({profile:profile,profile_detail:profileDetail});
  
    }catch(err){
        console.error(err.message);
        res.status(500).send('Server Error')
    }
  });
//@route post api/profile
// @desc add instructor detail
router.post(
    '/',
    [
       auth,
       [
        
           check('instructors','instructor is required')
            .not()
            .isEmpty(),
            check('education','education is required')
      ]
    ],
    async(req,res)=>{
      const errors=validationResult(req);
      if(!errors.isEmpty()){
          return res.status(400).json({errors:errors.array()});
       }

       const{
           institute,
           website,
           location,
           bio,
           githubusername,
           skills,
           youtube,
           facebook,
           twitter,
           instagram,
           linkedin
       }=req.body;
  // Build profile object
  const profileFields={};
      profileFields.user=req.user.id;
      if(institute) profileFields.institute=institute;
      // if (website) profileFields.website = website;
      // if (location) profileFields.location = location;
      // if (bio) profileFields.bio = bio;
      // if (status) profileFields.status = status;
      // if (githubusername)
      //   profileFields.githubusername = githubusername;
      // Skills - Spilt into array
      if (typeof skills !== 'undefined') {
        profileFields.skills = skills.split(',');
      }
      console.log(profileFields.skills);
    try{
        let profile=await Profile.findOne({user:req.user.id})
        if(profile){
            profile=await Profile.findOneAndUpdate(
                {user:req.user.id},
                {$set:profileFields},
                {new:true}
            )
            return res.json(profile)
        }
      //  create
      profile=new Profile(profileFields);
      await profile.save()
      res.json(profile)
    }catch(err){
        console.error(err.message);
        res.status(500).send('Server Error')
    }

  }

)

//@route post api/student/
// @desc add new instructor
router.post('/:instructor_id',auth,
  async(req,res)=>{
  try{
    const instructorId=await User.findById(req.params.instructor_id)
    if(!instructorId) 
      return res.send('User not valid') 
    let profile=await Student.findOne({user:req.user.id})
    profile.instructors=instructorId;
    await profile.save()
    res.json(profile)
  }catch(err){
      console.error(err.message);
      res.status(500).send('Server Error')
  }

}

)
//@route delete api/student/
// @desc delete instructor
router.delete('/:instructor_id',auth,
  async(req,res)=>{
  try{
    const instructorId=await User.findById(req.params.instructor_id)
    if(!instructorId) 
      return res.send('User not valid') 
    let profile=await Student.findOne({user:req.user.id})
    delete profile.instructors.instructorId;
    await profile.save()
    res.json(profile)
  }catch(err){
      console.error(err.message);
      res.status(500).send('Server Error')
  }

}

)


module.exports=router;
