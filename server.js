const express= require('express')
const app=express();
const PORT=process.env.PORT|5000;
const connectDB=require("./config/db")

// connect database
connectDB();

// Init middleware
app.use(express.json()); //Used to parse JSON bodies

app.get('/',(req,res)=>res.send("Api running"));

// Define routers
app.use('/api/users',require('./routes/api/users'))
app.use('/api/posts',require('./routes/api/posts'))
app.use('/api/auth',require('./routes/api/auth'))
app.use('/api/profile',require('./routes/api/profile'))
app.use('/api/student',require('./routes/api/student'))
app.listen(PORT,()=>console.log(`server start port is ${PORT}`));